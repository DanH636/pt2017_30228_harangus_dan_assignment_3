package model;

public class Product {
	public int ID;
	public String Name;
	public int Price;
	public int Stock;
	

	public Product(){
		
	}
		
	public Product(int ID, String Name, int Price, int Stock){
		super();
		this.ID=ID;
		this.Name=Name;
		this.Price=Price;
		this.Stock=Stock;
	}
	
		public int getID() {
		return ID;
	}

	public void setID(int iD) {
		this.ID = iD;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		this.Name = name;
	}

	public int getPrice() {
		return Price;
	}

	public void setPrice(int price) {
		this.Price = price;
	}

	public int getStock() {
		return Stock;
	}

	public void setStock(int stock) {
		this.Stock = stock;
	}

	@Override
	public String toString(){
		return "Product [id=" + ID + ", name=" + Name + ", price=" + Price + ", stock=" + Stock ;
	}
	

}
