package model;

public class Client {
	private int ID;
	private String Name;
	private String Adress;
	private String Email;
	
	public Client(){
		
	}
	
	public Client(int ID, String Name, String Adress, String Email){
		super();
		this.ID=ID;
		this.Name=Name;
		this.Adress=Adress;
		this.Email=Email;
	}
	
	public int getID(){
		return ID;
	}
	
	public void setID(int ID){
		this.ID=ID;
	}
	
	public String getName(){
		return Name;
	}
	
	public void setName(String Name){
		this.Name=Name;
	}
	
	public String getAdress(){
		return Adress;
	}
	
	public void setAdress(String Adress){
		this.Adress=Adress;
	}
	
	public String getEmail(){
		return Email;
	}
	
	public void setEmail(String Email){
		this.Email=Email;
	}
	
	@Override
	public String toString(){
		return "Client [id=" + ID + ", name=" + Name + ", address=" + Adress + ", email=" + Email ;
	}
	

}
