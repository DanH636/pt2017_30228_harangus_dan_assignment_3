package model;

public class Orders {
	private int ID;
	private String Client;
	private String Product;
	private int Quantity;
	
	public Orders(){
		
	}
	
	public Orders(int ID, String Client, String Product, int Quantity){
		super();
		this.ID=ID;
		this.Client=Client;
		this.Product=Product;
		this.Quantity=Quantity;
	}
	
	public int getID(){
		return ID;
	}
	
	public void setID(int ID){
		this.ID=ID;
	}
	
	public String getClient(){
		return Client;
	}
	
	public void setClient(String Client){
		this.Client=Client;
	}
	
	public String getProduct(){
		return Product;
	}
	
	public void setProduct(String Product){
		this.Product=Product;
	}
	
	public int getQuantity(){
		return Quantity;
	}
	
	public void setQuantity(int Quantity){
		this.Quantity=Quantity;
	}
	
	@Override
	public String toString(){
		return "Order [id=" + ID + ", client=" + Client + ", product=" + Product + ", quantity=" + Quantity ;
	}
	


}
