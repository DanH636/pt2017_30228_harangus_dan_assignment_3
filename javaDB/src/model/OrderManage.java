package model;

public class OrderManage {
	public int ID;
	public String Product;
	public String Client;
	public int OrderID;
	
	public OrderManage(){
		
	}
	
	public OrderManage(int ID, String Product, String Client, int OrderID){
		this.ID=ID;
		this.Product=Product;
		this.Client=Client;
		this.OrderID=OrderID;
	}
	
	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		this.ID = iD;
	}

	public String getProduct() {
		return Product;
	}

	public void setProduct(String product) {
		this.Product = product;
	}

	public String getClient() {
		return Client;
	}

	public void setClient(String client) {
		this.Client = client;
	}

	public int getOrderID() {
		return OrderID;
	}

	public void setOrderID(int orderID) {
		this.OrderID = orderID;
	}
	
	@Override
	public String toString(){
		return "OrderManager [id=" + ID + ", product=" + Product + ", client=" + Client + ", orderID=" + OrderID ;
	}
	

	
}
