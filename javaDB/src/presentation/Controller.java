package presentation;

import java.util.ArrayList;
import java.util.List;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.OrderManageBLL;
import bll.ProductBLL;
import model.Client;
import model.Orders;
import model.OrderManage;
import model.Product;

public class Controller {
	
	private ClientBLL c;
	private ProductBLL p;
	private OrderBLL o;
	private OrderManageBLL om;
	
	public Controller(){
		c=new ClientBLL();
		p=new ProductBLL();
		o=new OrderBLL();
		om=new OrderManageBLL();
	}
	
	public List<Client> getAllClients(){
		return c.selectAll();
	}
	
	public List<Product> getAllProducts(){
		return p.selectAll();
	}
	
	public List<Orders> getAllOrders(){
		return o.selectAll();
		
	}
	
	public List<OrderManage> getAllOM(){
		return om.selectAll();
	}
	
	public List<Client> getClientID(int id){
		List<Client> listC=new ArrayList<Client>();
		listC.add(c.selectByID(id));
		return listC;
	}
	
	
	public List<Product> getProductID(int id){
		List<Product> listP=new ArrayList<Product>();
		listP.add(p.selectByID(id));
		return listP;
	}
	
	public List<Orders> getOrderID(int id){
		List<Orders> listO=new ArrayList<Orders>();
		listO.add(o.selectByID(id));
		return listO;
	}
	
	public List<OrderManage> getOmID(int id){
		List<OrderManage> listOM=new ArrayList<OrderManage>();
		listOM.add(om.selectByID(id));
		return listOM;
	}
	
	public List<OrderManage> getOmOID(int orderID){
		return om.selectByOID(orderID);

	}
	
	public List<OrderManage> getOmProd(String product){
		return om.selectByProduct(product);
	}
	
	public void deleteC(int id){
		c.deleteClient(id);
	}
	
	public void deleteP(int id){
		p.deleteP(id);
	}
	
	public void deleteO(int id){
		o.deleteByID(id);
	}
	
	public void deleteOM(int id){
		om.deleteOM(id);
	}
	
	public void insertC(Client cl){
		c.insertClient(cl);
	}
	
	public void insertP(Product prod){
		p.insertP(prod);
	}
	
	public String insertOrder(int idC, int idP, int stock){
		return o.createOrder(idC, idP, stock);
	}
	
	public void insertOM(OrderManage omg){
		om.insertOM(omg);
	}
	
	public void updateC(Client cl){
		c.updateClient(cl);
	}
	
	public void updateP(Product prod){
		p.updateP(prod);
	}

	public void updateO(Orders ord){
		o.updateOrder(ord);
	}
	
	public void updateOM(OrderManage omg){
		om.updateOM(omg);
	}
}
