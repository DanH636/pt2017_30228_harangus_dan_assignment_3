package presentation;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import model.Client;
import model.Orders;
import model.OrderManage;
import model.Product;

public class Panel {
	public JFrame frame;
	public JButton allC,allP,allO,allOM,selC,selP,selO,selOM,selOmOID,selOmProd,delC,delP,delO,delOM,insC,insP,insO,insOM,upC,upP,upO,upOM;
	public JPanel btn,cPanel,pPanel,oPanel,defPanel;
	public JTable table;
	Controller controller;
	
	public Panel(){
		
		controller=new Controller();
		btn=new JPanel();
		allC=new JButton("All Clients");
		allP=new JButton("All Products");
		allO=new JButton("All Orders");
		allOM=new JButton("All OM");
		selC=new JButton("Select Clients");
		selP=new JButton("Select Products");
		selO=new JButton("Select Orders");
		selOM=new JButton("Select OM");
		selOmOID=new JButton("Select OM by oID");
		selOmProd=new JButton("Select OM by Prod");
		delC=new JButton("Delete Clients");
		delP=new JButton("Delete Products");
		delO=new JButton("Delete Orders");
		delOM=new JButton("Delete OM");
		insC=new JButton("Insert Clients");
		insP=new JButton("Insert Products");
		insO=new JButton("Insert Orders");
		insOM=new JButton("Insert OM");
		upC=new JButton("Update Clients");
		upP=new JButton("Update Products");
		upO=new JButton("Update Orders");
		upOM=new JButton("Update OM");
		
		cPanel=new JPanel();
		pPanel=new JPanel();
		oPanel=new JPanel();
		defPanel=new JPanel();
		
		btn.add(allC);
		btn.add(allP);
		btn.add(allO);
		btn.add(allOM);
		btn.add(selC);
		btn.add(selP);
		btn.add(selO);
		btn.add(selOM);
		btn.add(selOmOID);
		btn.add(selOmProd);
		btn.add(delC);
		btn.add(delP);
		btn.add(delO);
		btn.add(delOM);
		btn.add(insC);
		btn.add(insP);
		btn.add(insO);
		btn.add(insOM);
		btn.add(upC);
		btn.add(upP);
		btn.add(upO);
		btn.add(upOM);
		
		frame=new JFrame("Client database simulation");
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1200, 800);
		frame.setVisible(true);
		frame.setLayout(new GridLayout(4,4));
		frame.add(btn);
		frame.add(defPanel);
		
		start();
		
	}
	
	public void start(){
		
		allC.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				
				List<Client> cList = controller.getAllClients();
				table = new JTable(new BeanTableModel<Client>(Client.class,cList));
				defPanel.removeAll();
				JScrollPane scroll = new JScrollPane();
				scroll.getViewport().add(table);
				defPanel.add(scroll);
				defPanel.updateUI();
				
			}
		});
		
		allP.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				
				List<Product> pList = controller.getAllProducts();
				table = new JTable(new BeanTableModel<Product>(Product.class,pList));
				defPanel.removeAll();
				JScrollPane scroll = new JScrollPane();
				scroll.getViewport().add(table);
				defPanel.add(scroll);
				defPanel.updateUI();
				
			}
		});
		
		allO.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				
				List<Orders> oList = controller.getAllOrders();
				table = new JTable(new BeanTableModel<Orders>(Orders.class,oList));
				defPanel.removeAll();
				JScrollPane scroll = new JScrollPane();
				scroll.getViewport().add(table);
				defPanel.add(scroll);
				defPanel.updateUI();
				
			}
		});
		
		allOM.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				
				List<OrderManage> omList = controller.getAllOM();
				table = new JTable(new BeanTableModel<OrderManage>(OrderManage.class,omList));
				defPanel.removeAll();
				JScrollPane scroll = new JScrollPane();
				scroll.getViewport().add(table);
				defPanel.add(scroll);
				defPanel.updateUI();
				
			}
		});
		
		selC.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(ptId);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						List<Client> cList = controller.getClientID(id);
						defPanel.removeAll();
						table = new JTable(new BeanTableModel<Client>(Client.class,cList));
						defPanel.add(new JScrollPane(table));
						defPanel.updateUI();
					}
				});
			}
		});
		
		selP.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(ptId);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						List<Product> pList = controller.getProductID(id);
						defPanel.removeAll();
						table = new JTable(new BeanTableModel<Product>(Product.class,pList));
						defPanel.add(new JScrollPane(table));
						defPanel.updateUI();
					}
				});
			}
		});
		
		
		selO.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(ptId);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						List<Orders> oList = controller.getOrderID(id);
						defPanel.removeAll();
						table = new JTable(new BeanTableModel<Orders>(Orders.class,oList));
						defPanel.add(new JScrollPane(table));
						defPanel.updateUI();
					}
				});
			}
		});
		
		selOM.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(ptId);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						List<OrderManage> omList = controller.getOmID(id);
						defPanel.removeAll();
						table = new JTable(new BeanTableModel<OrderManage>(OrderManage.class,omList));
						defPanel.add(new JScrollPane(table));
						defPanel.updateUI();
					}
				});
			}
		});
		
		selOmOID.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptOid = new JTextField(3);
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(ptOid);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptOid.getText());
						List<OrderManage> omList = controller.getOmOID(id);
						defPanel.removeAll();
						table = new JTable(new BeanTableModel<OrderManage>(OrderManage.class,omList));
						defPanel.add(new JScrollPane(table));
						defPanel.updateUI();
					}
				});
			}
		});
		
		selOmProd.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptProd = new JTextField(10);
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(ptProd);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						String prod = ptProd.getText();
						List<OrderManage> omList = controller.getOmProd(prod);
						defPanel.removeAll();
						table = new JTable(new BeanTableModel<OrderManage>(OrderManage.class,omList));
						defPanel.add(new JScrollPane(table));
						defPanel.updateUI();
					}
				});
			}
		});
		
		delC.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(ptId);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						defPanel.removeAll();
						controller.deleteC(id);
						defPanel.updateUI();
					}
				});
			}
		});
		
		delP.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(ptId);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						defPanel.removeAll();
						controller.deleteP(id);
						defPanel.updateUI();
					}
				});
			}
		});
		
		delO.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(ptId);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						defPanel.removeAll();
						controller.deleteO(id);
						defPanel.updateUI();
					}
				});
			}
		});
		
		delOM.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(ptId);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						defPanel.removeAll();
						controller.deleteOM(id);
						defPanel.updateUI();
					}
				});
			}
		});
		
		insC.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JTextField ptName = new JTextField(20);
				JTextField ptEmail = new JTextField(20);
				JTextField ptAddress = new JTextField(20);
				JLabel labId = new JLabel("ID");
				JLabel labName = new JLabel("Name");
				JLabel labAddress = new JLabel("Address");
				JLabel labEmail = new JLabel("Email");
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(labId);
				defPanel.add(ptId);
				defPanel.add(labName);
				defPanel.add(ptName);
				defPanel.add(labAddress);
				defPanel.add(ptAddress);
				defPanel.add(labEmail);
				defPanel.add(ptEmail);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						String name = ptName.getText();
						String address = ptAddress.getText();
						String email = ptEmail.getText();
						Client c = new Client(id,name,address,email);
						controller.insertC(c);
						defPanel.removeAll();
						defPanel.updateUI();
					}
				});
			}
		});	
		
		insP.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JTextField ptName = new JTextField(20);
				JTextField ptPrice = new JTextField(10);
				JTextField ptStock = new JTextField(3);
				JLabel labId = new JLabel("ID");
				JLabel labName = new JLabel("Name");
				JLabel labPrice = new JLabel("Price");
				JLabel labStock = new JLabel("Stock");
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(labId);
				defPanel.add(ptId);
				defPanel.add(labName);
				defPanel.add(ptName);
				defPanel.add(labPrice);
				defPanel.add(ptPrice);
				defPanel.add(labStock);
				defPanel.add(ptStock);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						String name = ptName.getText();
						int price = Integer.parseInt(ptPrice.getText());
						int stock = Integer.parseInt(ptStock.getText());
						
						Product p = new Product(id,name,price,stock);
						controller.insertP(p);
						defPanel.removeAll();
						defPanel.updateUI();
					}
				});
			}
		});	
		
		
		insO.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptClient = new JTextField(3);
				JTextField ptProduct = new JTextField(3);
				JTextField ptStock = new JTextField(3);
		
				JLabel labClient = new JLabel("Client ID");
				JLabel labProduct = new JLabel("Product ID");
				JLabel labStock = new JLabel("Quantity");
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(labClient);
				defPanel.add(ptClient);
				defPanel.add(labProduct);
				defPanel.add(ptProduct);
				defPanel.add(labStock);
				defPanel.add(ptStock);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						
						int client = Integer.parseInt(ptClient.getText());
						int product = Integer.parseInt(ptProduct.getText());
						int stock = Integer.parseInt(ptStock.getText());
						String rez = controller.insertOrder(client, product, stock);
						if(rez != null){
							JLabel msj = new JLabel(rez);
							defPanel.removeAll();
							defPanel.add(msj);
							defPanel.updateUI();
						}
						else{
							defPanel.removeAll();
							defPanel.updateUI();
							try{
								PrintWriter writer = new PrintWriter("Order.txt","UTF-8");
								List<Orders> oList = controller.getAllOrders();
								for(Orders o : oList){
									writer.println("Nr Order:"+ o.getID());
									writer.println("Client:" + o.getClient());
									writer.println("Product:" + o.getProduct());
									writer.println("Stock:" + o.getQuantity());
									writer.close();
								}
							}catch (IOException e){
									System.out.println("ERROR: file writing!");
								}
					
							}
						}
						
					
				});
			}
		});
		
		insOM.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JTextField ptProduct = new JTextField(20);
				JTextField ptClient = new JTextField(20);
				JTextField ptOrderID = new JTextField(3);
				JLabel labId = new JLabel("ID");
				JLabel labProduct = new JLabel("Product");
				JLabel labClient = new JLabel("Client");
				JLabel labOrderID = new JLabel("OrderID");
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(labId);
				defPanel.add(ptId);
				defPanel.add(labProduct);
				defPanel.add(ptProduct);
				defPanel.add(labClient);
				defPanel.add(ptClient);
				defPanel.add(labOrderID);
				defPanel.add(ptOrderID);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						String prod = ptProduct.getText();
						String client = ptClient.getText();
						int orderID = Integer.parseInt(ptOrderID.getText());
						OrderManage om = new OrderManage(id,prod,client,orderID);
						controller.insertOM(om);
						defPanel.removeAll();
						defPanel.updateUI();
					}
				});
			}
		});
		
		
		upC.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JTextField ptName = new JTextField(20);
				JTextField ptEmail = new JTextField(20);
				JTextField ptAddress = new JTextField(20);
				JLabel labId = new JLabel("ID");
				JLabel labName = new JLabel("Name");
				JLabel labAddress = new JLabel("Address");
				JLabel labEmail = new JLabel("Email");
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(labId);
				defPanel.add(ptId);
				defPanel.add(labName);
				defPanel.add(ptName);
				defPanel.add(labAddress);
				defPanel.add(ptAddress);
				defPanel.add(labEmail);
				defPanel.add(ptEmail);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						String name = ptName.getText();
						String address = ptAddress.getText();
						String email = ptEmail.getText();
						Client c = new Client(id,name,address,email);
						controller.updateC(c);
						defPanel.removeAll();
						defPanel.updateUI();
					}
				});
			}
		});
		
		upP.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptId = new JTextField(3);
				JTextField ptName = new JTextField(20);
				JTextField ptPrice = new JTextField(10);
				JTextField ptStock = new JTextField(3);
				JLabel labId = new JLabel("ID");
				JLabel labName = new JLabel("Name");
				JLabel labPrice = new JLabel("Price");
				JLabel labStock = new JLabel("Stock");
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(labId);
				defPanel.add(ptId);
				defPanel.add(labName);
				defPanel.add(ptName);
				defPanel.add(labPrice);
				defPanel.add(ptPrice);
				defPanel.add(labStock);
				defPanel.add(ptStock);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						String name = ptName.getText();
						int price = Integer.parseInt(ptPrice.getText());
						int stock = Integer.parseInt(ptStock.getText());
						
						Product p = new Product(id,name,price,stock);
						controller.updateP(p);
						defPanel.removeAll();
						defPanel.updateUI();
					}
				});
			}
		});
		
		upO.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptClient = new JTextField(20);
				JTextField ptProduct = new JTextField(10);
				JTextField ptStock = new JTextField(3);
				JTextField ptId = new JTextField(3);
				JLabel labClient = new JLabel("Client");
				JLabel labProduct = new JLabel("Product");
				JLabel labId = new JLabel("Id");
				JLabel labStock = new JLabel("Stock");
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(labId);
				defPanel.add(ptId);
				defPanel.add(labClient);
				defPanel.add(ptClient);
				defPanel.add(labProduct);
				defPanel.add(ptProduct);
				defPanel.add(labStock);
				defPanel.add(ptStock);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						String client = ptClient.getText();
						String product =ptProduct.getText();
						int stock = Integer.parseInt(ptStock.getText());
						controller.updateO(new Orders(id,client,product,stock));
						defPanel.removeAll();
						defPanel.updateUI();
					}
				});
			}
		});	
		
		
		upOM.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JTextField ptClient = new JTextField(20);
				JTextField ptProduct = new JTextField(10);
				JTextField ptOrderID = new JTextField(3);
				JTextField ptId = new JTextField(3);
				JLabel labClient = new JLabel("Client");
				JLabel labProduct = new JLabel("Product");
				JLabel labId = new JLabel("Id");
				JLabel labOrderID = new JLabel("OrderID");
				JButton ok = new JButton("ok");
				defPanel.removeAll();
				defPanel.add(labId);
				defPanel.add(ptId);
				defPanel.add(labClient);
				defPanel.add(ptClient);
				defPanel.add(labProduct);
				defPanel.add(ptProduct);
				defPanel.add(labOrderID);
				defPanel.add(ptOrderID);
				defPanel.add(ok);
				defPanel.updateUI();
				
				ok.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent t){
						int id = Integer.parseInt(ptId.getText());
						String client = ptClient.getText();
						String product = ptProduct.getText();
						int orderID = Integer.parseInt(ptOrderID.getText());
						controller.updateOM(new OrderManage(id,client,product,orderID));
						defPanel.removeAll();
						defPanel.updateUI();
					}
				});
			}
		});	






		
	}

}
