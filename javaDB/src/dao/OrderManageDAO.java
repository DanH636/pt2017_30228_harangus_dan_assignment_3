package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import connection.ConnectionFactory;
import model.OrderManage;

public class OrderManageDAO extends AbstractDAO<OrderManage> {
	
	public OrderManageDAO(){
		
	}
	
	public List<OrderManage> findByOID(int orderID){
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("orderID");
		try{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, orderID);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
			
		}catch(SQLException e){
			System.out.println("Problema in OrderManagerDAO");
		}finally{
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
	public List<OrderManage> findByProduct(String product){
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("product");
		try{
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1, product);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
			
		}catch(SQLException e){
			System.out.println("Problema in OrderManagerDAO");
		}finally{
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
}


