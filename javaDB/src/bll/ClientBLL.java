package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import dao.ClientDAO;
import bll.validators.*;
import model.Client;

public class ClientBLL {

	private List<Validator<Client>> validators;
	private ClientDAO clientDAO;
	
	public ClientBLL(){
		validators=new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
		
		clientDAO=new ClientDAO();
	}
		
	public List<Client> selectAll(){
		return clientDAO.findAll();
	}
	
	public Client selectByID(int id){
		return clientDAO.findById(id);
	}
	

	
	public void insertClient(Client c){
		clientDAO.insert(c);
	}
	
	public void updateClient(Client c){
		clientDAO.update(c);
	}
	
	public void deleteClient(int id){
		clientDAO.delete(id);
	}
}
