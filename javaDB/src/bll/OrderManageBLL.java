package bll;

import java.util.List;

import dao.OrderManageDAO;
import model.OrderManage;

public class OrderManageBLL {
	
	private OrderManageDAO om;
	
	public OrderManageBLL(){
		om=new OrderManageDAO();
	}
	
	public List<OrderManage> selectAll(){
		return om.findAll();
	}
	
	public OrderManage selectByID(int id){
		return om.findById(id);
	}
	
	public List<OrderManage> selectByOID(int orderID){
		return om.findByOID(orderID);
	}
	
	public List<OrderManage> selectByProduct(String Product){
		return om.findByProduct(Product);
	}
	
	public void insertOM(OrderManage o){
		om.insert(o);
	}
	
	public void updateOM(OrderManage o){
		om.update(o);
	}
	
	public void deleteOM(int id){
		om.delete(id);
	}

}
