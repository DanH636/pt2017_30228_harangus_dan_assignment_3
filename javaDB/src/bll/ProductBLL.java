package bll;

import java.util.List;

import dao.ProductDAO;
import model.Product;

public class ProductBLL {
	private ProductDAO p;
	
	public ProductBLL(){
		p=new ProductDAO();
	}
	
	public List<Product> selectAll(){
		return p.findAll();
	}
	
	public Product selectByID(int id){
		return p.findById(id);
	}
	
	public void insertP(Product prod){
		p.insert(prod);
	}
	public void updateP(Product prod){
		p.update(prod);
	}
	
	public void deleteP(int id){
		p.delete(id);
	}
}
