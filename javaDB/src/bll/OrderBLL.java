package bll;

import java.util.List;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Client;
import model.Orders;
import model.Product;

public class OrderBLL {
	
	private String msg;
	private ClientDAO c;
	private ProductDAO p;
	private OrderDAO o;
	private int nrO;
	
	public OrderBLL(){
		msg="Empty stock";
		c=new ClientDAO();
		p=new ProductDAO();
		o=new OrderDAO();
		nrO=1;	
	}
	
	public String getMsg(){
		return msg;
	}
	
	public String createOrder(int idC, int idP, int quantity){
		Product prod;
		Client client;
		prod=p.findById(idP);
		
		if(prod.getStock()>=quantity){
			client=c.findById(idC);
			List<Orders> ord=o.findAll();
			if(ord.isEmpty()){
				o.insert(new Orders(1,client.getName(),prod.getName(),quantity));
			}
			else{
				nrO=ord.get(ord.size()-1).getID();
				nrO++;
				o.insert(new Orders(nrO,client.getName(),prod.getName(),quantity));
			}

			p.update(new Product(prod.getID(),prod.getName(),prod.getPrice(),prod.getStock()-quantity));
			
			return null;
		}
		else
			
			return msg;
	}
	
	public List<Orders> selectAll(){
		return o.findAll();
	}
	
	public Orders selectByID(int id){
		return o.findById(id);
	}
	
	public void deleteByID(int id){
		o.delete(id);
	}
	
	public void updateOrder(Orders ord){
		o.update(ord);
	}
}
